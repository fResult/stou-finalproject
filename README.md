# STOU FINAL GROUP Project

## About Project

โปรเจ็กต์นี้ เป็นส่วนหนึ่งของวิชา 99409 ประสบการณ์วิชาชีพเทคโนโลยีสารสนเทศและการสื่อสาร
ของสาขาวิชาเอกวิทยาการคอมพิวเตอร์ มหาวิทยาลัยสุโขทัยธรรมาธิราช
โดยมี **ท่าน รองศาสตราจารย์ ณัฏฐพร พิมพายน**
และ **ผู้ช่วยศาสตราจารย์ ดร. พิมผกา ประเสริฐศิลป์**
เป็นอาจารย์ที่ปรึกษาโครงการ

## Member

1. 5996004619 นาย ศิลา เศรษฐกาลอนันต์
2. 5996005731 นาย ภูชิต คารกูล
3. 5996009402 นาย ไพรรัตน์ อิ่นชัย
4. 5996009592 ว่าที่ รต. เชิดศักดิ์ เขิมสูงเนิน
5. 5996010046 นาย นพณัฐ หัถปัด
6. 5996011770 น.ส. พรสุดา นาเลิง

## Presentation
[Download Slide](http://bit.ly/3avvwzc)

## Get Started

1. Download as .zip file or use `git clone https://gitlab.com/fResult/stou-finalproject.git`
2. Open project `landTax` in NetBeans IDE
3. Run below SQL Script in phpMyAdmin or MySQL WorkBench or DBeaver or another
	```mysql
	CREATE DATABASE land_tax;  
	
	CREATE TABLE persons (  
 	   id_card_no VARCHAR(13),  
  	   first_name VARCHAR(50) NOT NULL,  
       last_name VARCHAR(60) NOT NULL,  
       address VARCHAR(300) NOT NULL,  
       phone_no VARCHAR(10) NOT NULL,  
       PRIMARY KEY (id_card_no)  
	);  

	CREATE TABLE land_statuses (
	   status_no INT,  
       name VARCHAR(40) NOT NULL,  
       PRIMARY KEY (status_no)  
	);  

	CREATE TABLE deeds (  
	   deed_no INT,  
       province VARCHAR(40) NOT NULL,  
       land_size INT NOT NULL,  
       price INT NOT NULL,  
       person_id VARCHAR(13) NOT NULL,  
       status_no INT NOT NULL,  
       PRIMARY KEY (deed_no),  
       FOREIGN KEY (person_id) REFERENCES persons(id_card_no),  
       FOREIGN KEY (status_no) REFERENCES land_statuses(status_no)  
	);  
	
	INSERT INTO persons(id_card_no, first_name, last_name, phone_no, address)  
 	VALUES ('1111', 'อัษฎางค์', 'นอนกลางแจ้ง','1938498', '19/159'),  
	   ('12234', 'oairat', 'jhfu', '0988766', '13m1'),  
 	   ('1234', 'รุจิรา', 'อย่าหมางเมิน', '09183101', '25/12 '),  
       ('2222', 'สวัสดี', 'วันจันทร์', '8888888888', '192/55'),  
       ('8877', 'อัศว', 'อาณาจักรพินาศ', '000000000', '99/999'),  
       ('9876', 'ชวกร', 'หอนทั้งคืน', '9999999999', '1394/10');  
    
 	INSERT INTO land_statuses VALUES (1, 'ที่ดินมีสิ่งปลูกสร้าง'), (2, 'ที่ดินว่างเปล่า');
 	
	INSERT INTO deeds  
	VALUES (11, 'บุรีรัมย์', 100000, 10.50, '1111', 2),  
	   (22, 'กรุงเทพ', 72,6.00, '1234', 1),  
	   (33, 'เชียงใหม่', 100000, 100.00, '1111', 1),  
	   (44, 'สกลนคร', 150000, 50.00, '1111', 2),  
	   (55, 'สมุทรสาคร', 50, 1.50, '1234', 1),  
 	   (66, 'สิงหบุรี', 12000, 20.00, '1234', 2);
	```
4. Let's run Program in NetBeans and try...
