CREATE DATABASE land_tax;

CREATE TABLE persons (
 id_card_no VARCHAR(13),
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(60) NOT NULL,
    address VARCHAR(300) NOT NULL,
    phone_no VARCHAR(10) NOT NULL,
    PRIMARY KEY (id_card_no)
);

CREATE TABLE land_statuses (
 status_no INT,
    name VARCHAR(40) NOT NULL,
    PRIMARY KEY (status_no)
);

CREATE TABLE deeds (
 deed_no INT,
    province VARCHAR(40) NOT NULL,
    land_size INT NOT NULL,
    price INT NOT NULL,
    person_id VARCHAR(13) NOT NULL,
    status_no INT NOT NULL,
    PRIMARY KEY (deed_no),
    FOREIGN KEY (person_id) REFERENCES persons(id_card_no),
    FOREIGN KEY (status_no) REFERENCES land_statuses(status_no)
);

