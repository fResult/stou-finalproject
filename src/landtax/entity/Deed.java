/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package landtax.entity;

/**
 *
 * @author fResult
 */
public class Deed {
    
    private String deedNo;
    private String province;
    private int landSize;
    private double price;
    private String status;

    public Deed(String deedNo, String province, int landSize, double price, String status) {
        this.deedNo = deedNo;
        this.province = province;
        this.landSize = landSize;
        this.price = price;
        this.status = status;
    }

    public String getDeedNo() {
        return deedNo;
    }

    public void setDeedNo(String deedNo) {
        this.deedNo = deedNo;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getLandSize() {
        return landSize;
    }

    public void setLandSize(int landSize) {
        this.landSize = landSize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 1) {
            throw new NumberFormatException("Price must be 1 or more.");
        }
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
