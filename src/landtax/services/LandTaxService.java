/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package landtax.services;

import java.util.List;
import java.util.function.Consumer;
import landtax.entity.Deed;

/**
 *
 * @author fResult
 */
public class LandTaxService {

    public double calculatePercentage(Deed deed) {
        double percentage = 0;
        double price = deed.getPrice();
        if ("ที่ดินมีสิ่งปลูกสร้าง".equals(deed.getStatus())) {

            if (price > 100) {
                percentage = 0.1;
            } else if (price > 75) {
                percentage = 0.05;
            } else if (price > 50) {
                percentage = 0.03;
            } else {
                percentage = 0;
            }

        } else {
            if (price > 5000) {
                percentage = 0.7;
            } else if (price > 1000) {
                percentage = 0.6;
            } else if (price > 200) {
                percentage = 0.5;
            } else if (price > 50) {
                percentage = 0.4;
            } else {
                percentage = 0.3;
            }
        }

        return percentage;
    }

    public double calculateLandTaxPrice(double price, double percentage) {
        return price * percentage / 100 * 100_0000;
    }

    public double calculateTotalTaxPrice(List<Deed> deeds) {
        double totalTaxPrice = 0;
        for (Deed deed: deeds) {
            double percentage = calculatePercentage(deed);
            totalTaxPrice += ((deed.getPrice() * percentage / 100) * 1000000);
        }
        return totalTaxPrice;
    }
}
